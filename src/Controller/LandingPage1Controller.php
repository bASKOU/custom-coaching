<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LandingPage1Controller extends AbstractController
{
    const CALENDY="https://calendly.com/natacha-bonnal/customcoaching";
    private $user;
    private $FullUser;
    private $ProfilMatching;

    private function CheckConnnection($request, $Key)
    {
        $CurrentClid = null;
        $clidTest = null;
        $em = $this->getDoctrine()->getManager();
        if ($Key !== null) {
            $TabC = explode('_', $Key);
            $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` ='" . $TabC[0]."'";
            $res = $em->getConnection()->FetchAll($sql);
            if (isset($res[0]['motdepassUser'])) {
                $clidTest = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            }

            if ($clidTest == $Key) {
                $CurrentClid = $clidTest;
                $duree = time() + 3600 * 24 * 180;
                $cookie = new Cookie('CL', $CurrentClid, $duree);
                $response = new Response();
                $response->headers->setCookie($cookie);
                $response->send();
            }
        } else {
            $cookies = $request->cookies;
            if ($cookies->has('CL')) {
                $TabC = explode('_', $cookies->get('CL'));
                $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =" . $TabC[0];
                $res = $em->getConnection()->FetchAll($sql);
                if (isset($res[0]['motdepassUser'])) {
                    $clidTest = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
                    if ($clidTest == $cookies->get('CL')) {
                        $CurrentClid = $clidTest;
                    }
                }
            }
        }

        if ($CurrentClid !== null) {
            $TabC = explode('_', $CurrentClid);
            // var_dump($TabC);
            $this->user = $TabC[0];
            $this->FullUser = $res[0];

            test:
            $sql = "SELECT * FROM `utilisateur_profilmatching` WHERE `idUser` =" . $TabC[0];
            $res = $em->getConnection()->FetchAll($sql);
            if ($res == null) {
                $sql = "INSERT INTO `utilisateur_profilmatching` (`IdUser`, `route`) VALUES ('" . $TabC[0] . "', 'matching_Lp1_start');";
                $em->getConnection()->query($sql);
                goto test;
            }
            $this->ProfilMatching = $res[0];

        }
    }

    /**
     * @Route("OFFF/lp1/create/startXXcXX/{id}", name="matching_Lp1_index_create_start")
     */
    public function CreateStart($id, Request $request)
    {
        exit;
        $em = $this->getDoctrine()->getManager();
        $test = $request->get('RETEST');

        if ($test !== '') {
            $sql = "DELETE FROM  `utilisateur_profilmatching`  WHERE `idUser` =$id ";
            $em->getConnection()->query($sql);
        }

        $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =$id ";
        $res = $em->getConnection()->FetchAll($sql);
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_Lp1_index_start', array('key' => $cle)));
        }
    }




    /**
     * @Route("/lp1/start/{key}", name="matching_Lp1_index_start")
     */
    public function Start($key, Request $request)
    {
        $this->CheckConnnection($request, $key);
        //var_dump($this->user);
        //var_dump($this->FullUser);
        //var_dump($this->ProfilMatching);

        if ($this->ProfilMatching == null) {
            return $this->redirect($this->generateUrl('autentification_fausse'));
            }



        return $this->redirect($this->generateUrl($this->ProfilMatching['route']));
    }

    /**
     * @Route("/lp1/rdv/{key}", name="matching_Lp1_rdvredirect")
     */
    public function RdvRedirect($key, Request $request)
    {
        $this->CheckConnnection($request, $key);
        //var_dump($this->user);
        //var_dump($this->FullUser);
        //var_dump($this->ProfilMatching);

        if ($this->ProfilMatching == null) {
            return $this->redirect($this->generateUrl('autentification_fausse'));
        }
        return $this->redirect($this->generateUrl('matching_Lp1_Redirect'));
    }



    public function setValue($champs, $valeur)
    {
        $em = $this->getDoctrine()->getManager();
        $sql = "UPDATE utilisateur_profilmatching SET  `$champs`='$valeur' WHERE `idUser`=" . $this->user;
        $em->getConnection()->query($sql);
    }

    public function FullTest($CurrentRoute, Request $request)
    {
        $this->CheckConnnection($request, null);

        if ($this->ProfilMatching == null) {
            return $this->generateUrl('autentification_fausse');
        }

        if ($CurrentRoute !== $this->ProfilMatching['route']) {
            return $this->generateUrl($this->ProfilMatching['route']);
        }

        return null;
    }


    /**
     * @Route("/Lp1_Start", name="matching_Lp1_start")
     */
    public function index(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('objectif', $choix);

            if ($choix == 'commencer') {
                $this->setValue('route', 'matching_Lp1_index');
                return $this->redirect($this->generateUrl('matching_Lp1_index'));
            }


            if ($choix == 'rdv') {
                return $this->redirect($this->generateUrl('matching_Lp1_Redirect'));

            }
        }

        return $this->render('landing_page1/index.html.twig', [
            'mail'=>$this->FullUser['emailUser']]
        );
    }


    /**
     * @Route("/Lp1_index/", name="matching_Lp1_index")
     */
    public function Landing(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('objectif', $choix);

            if ($choix == 'poids') {
                $this->setValue('route', 'matching_Lp1_Poids');
                return $this->redirect($this->generateUrl('matching_Lp1_Poids'));
            }

            if ($choix == 'sante') {
                $this->setValue('route', 'matching_Lp1_Sante_sports');
                return $this->redirect($this->generateUrl('matching_Lp1_Sante_sports'));
            }

            if ($choix == 'perf') {
                $this->setValue('route', 'matching_Lp1_Perfo_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Lp1_Perfo_sports_pratique'));
            }

            if ($choix == 'muscle') {
                $this->setValue('route', 'matching_Lp1_Perfo_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Lp1_Perfo_sports_pratique'));
            }
        }

        return $this->render('matching/index.html.twig', [
            'mail'=>$this->FullUser['emailUser']]
            );
    }

    /**
     * @Route("/lp1/Poids", name="matching_Lp1_Poids")
     */
    public function Poids(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('kilos_a_perdre', $choix);

            $this->setValue('route', 'matching_Lp1_Poids_sports');
            return $this->redirect($this->generateUrl('matching_Lp1_Poids_sports'));
        }

        return $this->render('matching/poids.combien.twig', [
            'mail'=>$this->FullUser['emailUser']
            ]);
    }


    /**
     * @Route("/lp1/Sante/sport/", name="matching_Lp1_Sante_sports")
     */
    public function SanteSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');

        if ($choix !== null) {
            if ($choix == 'Oui') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_Lp1_Sante_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Lp1_Sante_sports_pratique'));
            }

            if ($choix == 'Non') {
                $this->setValue('fait_du_sport', 0);
                $this->setValue('route', 'matching_Lp1_Sante_sports_souhait');
                return $this->redirect($this->generateUrl('matching_Lp1_Sante_sports_souhait'));
            }
        }

        return $this->render('matching/poids.sport.twig', [
            'type' => 'Sante'
            ,'mail'=>$this->FullUser['emailUser']

        ]);
    }

    /**
     * @Route("/lp1/Poids/sport/", name="matching_Lp1_Poids_sports")
     */
    public function PoidsSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');

        if ($choix !== null) {

            if ($choix == 'Oui') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_Lp1_Poids_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Lp1_Poids_sports_pratique'));
            }

            if ($choix == 'Non') {
                $this->setValue('fait_du_sport', 0);
                $this->setValue('route', 'matching_Lp1_Poids_sports_souhait');
                return $this->redirect($this->generateUrl('matching_Lp1_Poids_sports_souhait'));
            }


        }

        return $this->render('matching/poids.sport.twig', [
            'type' => 'Poids',
            'mail'=>$this->FullUser['emailUser']


        ]);
    }

    /**
     * @Route("/lp1/Sante/sport/souhait", name="matching_Lp1_Sante_sports_souhait")
     * @Route("/lp1/Poids/sport/souhait", name="matching_Lp1_Poids_sports_souhait")
     */
    public function PoidsSportsSouhait(Request $request)
    {
        $choix = $request->get('choix');
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        if ($choix !== null) {

            if ($choix == 'Oui') {
                $this->setValue('sport_souhaite_faire', 1);

                if ($currentRoute == 'matching_Lp1_Sante_sports_souhait') {

                    $this->setValue('route', 'matching_Lp1_Sante_sports_pratique_Souhait');
                    return $this->redirect($this->generateUrl('matching_Lp1_Sante_sports_pratique_Souhait'));
                } else {
                    $this->setValue('route', 'matching_Lp1_Poids_sports_pratique_Souhait');
                    return $this->redirect($this->generateUrl('matching_Lp1_Poids_sports_pratique_Souhait'));
                }
            }

            if ($choix == 'Non') {
                $this->setValue('sport_souhaite_faire', 0);
                $this->setValue('route', 'matching_Lp1_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_Lp1_RegimeAlimentaire'));
            }
        }

        return $this->render('matching/poids.sport.souhait.twig', [
            'currentRoute' => $currentRoute,
            'mail'=>$this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/lp1/Perfo/sport/pratique", name="matching_Lp1_Perfo_sports_pratique")
     * @Route("/lp1/Sante/sport/pratique", name="matching_Lp1_Sante_sports_pratique")
     * @Route("/lp1/Poids/sport/pratique", name="matching_Lp1_Poids_sports_pratique")
     */
    public function PoidsPratiqueSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }
        $formBuilder = $this->createFormBuilder();

        $formBuilder
            ->add('sports', ChoiceType::class, array('required' => true, 'multiple' => true, 'expanded' => true, 'choices' => ['Course à pied' => 'Course à pied', 'CrossFit' => 'CrossFit', 'Musculation' => 'Musculation', 'Vélo' => 'vélo', 'Triathlon' => 'triathlon', 'Bodybuilding' => 'Bodybuilding', 'Autre' => 'Autre']))
            ->add('autre', TextareaType::class, array('required' => false, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Valider'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (($form->isSubmitted()) and ($form->isValid())) {
            if (!isset($_POST['form']['sports'])) {
                goto suite;
            }

            $this->setValue('sport_pratique', addslashes(json_encode($_POST['form']['sports'])));
            $this->setValue('sport_pratique_autre', addslashes($_POST['form']['autre']));


            if ($currentRoute == 'matching_Lp1_Perfo_sports_pratique') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_Lp1_Perfo_sports_experience_pratique');
                return $this->redirect($this->generateUrl('matching_Lp1_Perfo_sports_experience_pratique'));
            } else {
                $this->setValue('route', 'matching_Lp1_Sante_sports_experience_pratique');
                return $this->redirect($this->generateUrl('matching_Lp1_Sante_sports_experience_pratique'));
            }
        }


        suite:
        return $this->render('matching/sport.pratique.twig', [
            'form' => $form->createView(),
            'currentRoute' => $currentRoute,
            'mail'=>$this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/lp1/Perfo/sport/experience_pratique", name="matching_Lp1_Perfo_sports_experience_pratique")
     * @Route("/lp1/Sante/experience_pratique", name="matching_Lp1_Sante_sports_experience_pratique")
     */
    public function PoidsPratiqueExperienceSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('Experience_sport', $choix);
            if ($currentRoute == 'matching_Lp1_Perfo_sports_experience_pratique') {
                $this->setValue('route', 'matching_Lp1_Perfo_sports_objectif');
                return $this->redirect($this->generateUrl('matching_Lp1_Perfo_sports_objectif'));
            } else {
                $this->setValue('route', 'matching_Lp1_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_Lp1_RegimeAlimentaire'));
            }

        }

        return $this->render('matching/sport.experience.twig', [
            'mail'=>$this->FullUser['emailUser']

        ]);
    }


    /**
     * @Route("/lp1/Perfo/sport/objectif", name="matching_Lp1_Perfo_sports_objectif")
     */
    public function PerfSportObjectif(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('sport_objectif', $choix);

            if ($choix == 'competition') {
                $this->setValue('route', 'matching_Lp1_Perfo_datecompetition');
                return $this->redirect($this->generateUrl('matching_Lp1_Perfo_datecompetition'));
            }

            if ($choix == 'perf_sportive') {
                $this->setValue('route', 'matching_Lp1_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_Lp1_RegimeAlimentaire'));
            }

            if ($choix == 'esthetique') {
                $this->setValue('route', 'matching_Lp1_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_Lp1_RegimeAlimentaire'));
            }


        }

        return $this->render('matching/perf.objectif.twig', [
            'mail'=>$this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/lp1/Perfo/sport/datecompetition", name="matching_Lp1_Perfo_datecompetition")
     */
    public function PerfDateCompetition(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('delai_competition', $choix);

            $this->setValue('route', 'matching_Lp1_RegimeAlimentaire');
            return $this->redirect($this->generateUrl('matching_Lp1_RegimeAlimentaire'));

        }

        return $this->render('matching/perf.datecompetition.twig', [
            'mail'=>$this->FullUser['emailUser']
        ]);
    }

    /**
     * @Route("/lp1/Sante/sport/pratique_souhait", name="matching_Lp1_Sante_sports_pratique_Souhait")
     * @Route("/lp1/Poids/sport/pratique_souhait", name="matching_Lp1_Poids_sports_pratique_Souhait")
     */
    public function PoidsPratiqueSportsSouhait(Request $request)
    {
        $formBuilder = $this->createFormBuilder();
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $formBuilder
            ->add('sports', ChoiceType::class, array('required' => true, 'multiple' => true, 'expanded' => true, 'choices' => ['Course à pied' => 'Course à pied', 'CrossFit' => 'CrossFit', 'Musculation' => 'Musculation', 'Vélo' => 'vélo', 'Triathlon' => 'triathlon', 'Bodybuilding' => 'Bodybuilding', 'Autre' => 'Autre']))
            ->add('autre', TextareaType::class, array('required' => false, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Valider'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (($form->isSubmitted()) and ($form->isValid())) {
            $this->setValue('sport_souhait', addslashes(json_encode($_POST['form']['sports'])));
            $this->setValue('sport_souhait_autre', addslashes($_POST['form']['autre']));

            $this->setValue('route', 'matching_Lp1_RegimeAlimentaire');

            return $this->redirect($this->generateUrl('matching_Lp1_RegimeAlimentaire')

            );
        }


        return $this->render('matching/poids.sport.pratique.souhait.twig', [
            'form' => $form->createView(),
            'currentRoute' => $currentRoute,
            'mail'=>$this->FullUser['emailUser']

        ]);
    }


    /**
     * @Route("/lp1/Regime_Alimentaire", name="matching_Lp1_RegimeAlimentaire")
     */
    public function RegimeAlimentaire(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('regime_alimentaire', $choix);
            $this->setValue('route', 'matching_Lp1_HommeFemme');

            return $this->redirect($this->generateUrl('matching_Lp1_HommeFemme'));
        }

        return $this->render('matching/RegimeAlimentaire.twig', [
            'currentRoute' => $currentRoute,
            'mail'=>$this->FullUser['emailUser']


        ]);
    }

    /**
     * @Route("/lp1/HommeFemme", name="matching_Lp1_HommeFemme")
     */
    public function HommeFemme(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('Homme_Femme', $choix);
            $this->setValue('route', 'matching_Lp1_Finish');

            return $this->redirect($this->generateUrl('matching_Lp1_Finish'));
        }

        return $this->render('matching/poids.HommeFemme.twig', [
            'mail'=>$this->FullUser['emailUser']

        ]);
    }

    /**
     * @Route("/lp1/Finish", name="matching_Lp1_Finish")
     */
    public function Finish(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');
        return $this->render('landing_page1/Finish.twig', [
            'mail'=>$this->FullUser['emailUser']

        ]);
    }

    /**
     * @Route("/lp1/Merci", name="matching_Lp1_Merci")
     */
    public function Merci(Request $request)
    {
        return $this->render('landing_page1/Merci.twig', ['Lien'=>self::CALENDY]);
    }



    /**
     * @Route("/lp1/Redirect", name="matching_Lp1_Redirect")
     */
    public function RedirectLp(Request $request)
    {
        return $this->render('landing_page1/Redirect.twig', ['Lien'=>self::CALENDY]);
    }

    /**
     * @Route("/lp1/ChoixCoach", name="matching_Lp1_ChoixCoach")
     */
    public function ChoixCoach(Request $request)
    {
        $choix = $request->get('choix');
        if ($choix == 'rdv') {
            return $this->redirect($this->generateUrl('matching_Lp1_Redirect'));
        }

        return $this->render('landing_page1/ChoixCoach.twig', ['mail'=>$this->FullUser['emailUser']]);
    }

    /**
     * @Route("/lp1/Calcul", name="matching_Lp1_Calcul")
     */
    public function Calcul(Request $request)
    {
        $debug = false;
        //$debug =true;

        $TabCoach = array();
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        $em = $this->getDoctrine()->getManager();

        $coaching = '1';
        $nutrition = '1';

        $sql = "SELECT REQ.* FROM (SELECT PR.*,100 AS SCORE,AD.etat FROM `coach_profilmatching` PR LEFT JOIN ccp.admin AD ON (AD.idAdmin=PR.idcoach) WHERE  `idcoach` > 0  ";
        if ($coaching == '1') {
            $sql .= ' AND coach_sport=1 ';
        }

        if ($nutrition == '1') {
            $sql .= ' AND coach_nutrition=1 ';
        }
        $sql .= ' ) as REQ WHERE REQ.etat=1';

        //Check si Actif
        //Echo $sql;
        //Exit;

        $objectif = $this->ProfilMatching['objectif'];
        $PreferenceSexe = $this->ProfilMatching['Homme_Femme'];
        $fait_du_sport = $this->ProfilMatching['fait_du_sport'];
        $sport_pratique = json_decode($this->ProfilMatching['sport_pratique']);
        $sport_souhaite_faire = $this->ProfilMatching['sport_souhaite_faire'];
        $sport_souhait = json_decode($this->ProfilMatching['sport_souhait']);

        $regime = $this->ProfilMatching['regime_alimentaire'];
        if ($regime == 'flexitarien') {
            $regime = 'généraliste';
        }

        if ($debug) {
            var_dump($objectif, $this->FullUser);
        }
        $Sexe = strtolower($this->FullUser['sexe']);
        $sport_objectif = $this->ProfilMatching['sport_objectif'];
        $Experience_sport= $this->ProfilMatching['Experience_sport'];

        $TestSportNutri = false;
        $TestSportPerf = false;
        if ($objectif == 'poids') {
            $OBJcoach = 'perte de poids ' . $Sexe;
            $TestSportNutri = true;
        }

        if ($objectif == 'sante') {
            $OBJcoach = 'recherche de bien-être';
            $TestSportNutri = true;
        }

        if ($objectif == 'perf') {
            $OBJcoach = 'préparation physique générale ' . $Sexe;
            $TestSportPerf = true;
        }

        if ($objectif == 'muscle') {
            $OBJcoach = 'prise de muscle ' . $Sexe;
            $TestSportPerf = true;
        }



        $Niveau = 'débutant';
        if ($Experience_sport>1) {$Niveau = 'intermédiaire';}
        if ($Experience_sport>3) {$Niveau = 'confirmé';}
        if (($objectif == 'perf') or ($objectif == 'muscle')) {
            if ($sport_objectif == 'competition') {
                $Niveau = 'compétition';
            }
        }

        $res = $em->getConnection()->FetchAll($sql);

        if ($debug) {
            var_dump($this->ProfilMatching);
            var_dump('----');
        }
        foreach ($res as $R) {
            $SCORE = $R['SCORE'];
            //Ponderation sur le fait qu'il propose du coaching en plus que ce qui est demandé...
            if (!($nutrition == $R['coach_nutrition'])) {
                $SCORE = $SCORE * 0.6;
            }

            if (!($coaching == $R['coach_sport'])) {
                $SCORE = $SCORE * 0.6;
            }


            if ($Niveau == 'compétition') {
                if (strpos($R['niveau'], $Niveau) !== false) {
                    if (strpos($R['objectifs'], $OBJcoach) !== false) {
                        $SCORE = $SCORE * 2;
                    } else {
                        $SCORE = 0;
                        goto fin;
                    }
                }
            }


            if (strpos($R['objectifs'], $OBJcoach) !== false) {
                $SCORE = $SCORE * sqrt(1 / $R['objectifs_nbr']);
            } else {
                $SCORE = 0;
                goto fin;

            }

            if ($nutrition == '1') {
                if (strpos($R['regime'], $regime) !== false) {
                    $SCORE = $SCORE * sqrt(1 / $R['regime_nbr']);
                } else {
                    $SCORE = $SCORE * 0.1;
                }
            }


            if ($objectif == 'poids') {
            }

            if ($objectif == 'sante') {
            }

            if ($fait_du_sport=='1') {

                if (strpos($R['niveau'], $Niveau) !== false) {
                    $SCORE = $SCORE * sqrt(1 / $R['niveau_nbr']);
                } else {
                    $SCORE = $SCORE * 0.2;
                }


                if ($TestSportNutri == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_nutritionnel'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_nutritionnel_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
                if ($TestSportPerf == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_sportif'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_sportif_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
            } elseif ($sport_souhaite_faire == '1') {
                if ($TestSportNutri == true) {
                    foreach ($sport_souhait as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_nutritionnel'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_nutritionnel_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
                if ($TestSportPerf == true) {
                    foreach ($sport_souhait as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_sportif'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_sportif_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
            }

            if ($PreferenceSexe !== 'les deux') {
                if (strtolower($PreferenceSexe) !== strtolower($R['sexe_coach'])) {
                    $SCORE = $SCORE * 0.5;
                }
            }


            //On recupere les Coachs qui correspondent aux crietes Primaire

            fin:
            $R['SCORE'] = $SCORE;
            if ($SCORE > 0) {
                $TabCoach[] = $R;
            }
            if ($debug) {
                var_dump($R);
            }
        }

        $sort = array_column($TabCoach, 'SCORE');
        array_multisort($sort, SORT_DESC, $TabCoach);
        if (sizeof($TabCoach)>0) {
            while ($TabCoach[0]['SCORE'] < 90) {
                for ($i = 0; $i < sizeof($TabCoach); $i++) {
                    $TabCoach[$i]['SCORE'] = $TabCoach[$i]['SCORE'] * 1.1;
                }
            }
        }
        if ($debug) {
            var_dump($TabCoach);
        }
        $TabCoachFinal = array();
        $Nb = min(sizeof($TabCoach), 3);
        for ($i = 0; $i < $Nb; $i++) {
            $sql = "SELECT *,'" . $TabCoach[$i]['SCORE'] . "' AS SCORE FROM ccp.`admin` WHERE `idAdmin`=" . $TabCoach[$i]['idcoach'];
            $res2 = $em->getConnection()->FetchAll($sql);

            $TabCoachFinal[] = $res2[0];
        }
        //   var_dump($this->FullUser['token_clt']);
        return $this->render('landing_page1/Coachs.twig', [
            'Coachs' => $TabCoachFinal,
            'Token' =>$this->FullUser['token_clt'],
            'mail'=>$this->FullUser['emailUser']
        ]);
    }
}
