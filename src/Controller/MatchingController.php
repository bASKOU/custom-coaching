<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MatchingController extends AbstractController
{
    private $user;
    private $FullUser;
    private $ProfilMatching;

    private function CheckConnnection($request, $Key)
    {
        $CurrentClid = null;
        $clidTest = null;
        $em = $this->getDoctrine()->getManager();
        if ($Key !== null) {
            $TabC = explode('_', $Key);
            $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =" . $TabC[0];
            $res = $em->getConnection()->FetchAll($sql);
            if (isset($res[0]['motdepassUser'])) {
                $clidTest = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            }

            if ($clidTest == $Key) {
                $CurrentClid = $clidTest;
                $duree = time() + 3600 * 24 * 180;
                $cookie = new Cookie('CL', $CurrentClid, $duree);
                $response = new Response();
                $response->headers->setCookie($cookie);
                $response->send();
            }
        } else {
            $cookies = $request->cookies;
            if ($cookies->has('CL')) {
                $TabC = explode('_', $cookies->get('CL'));
                $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =" . $TabC[0];
                $res = $em->getConnection()->FetchAll($sql);
                if (isset($res[0]['motdepassUser'])) {
                    $clidTest = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
                    if ($clidTest == $cookies->get('CL')) {
                        $CurrentClid = $clidTest;
                    }
                }
            }
        }

        if ($CurrentClid !== null) {
            $TabC = explode('_', $CurrentClid);
            // var_dump($TabC);
            $this->user = $TabC[0];
            $this->FullUser = $res[0];

            test:
            $sql = "SELECT * FROM `utilisateur_profilmatching` WHERE `idUser` =" . $TabC[0];
            $res = $em->getConnection()->FetchAll($sql);
            if ($res == null) {
                $sql = "INSERT INTO `utilisateur_profilmatching` (`IdUser`, `route`) VALUES ('" . $TabC[0] . "', 'matching_index');";
                $em->getConnection()->query($sql);
                goto test;
            }
            $this->ProfilMatching = $res[0];

        }
    }

    /**
     * @Route("/matching/create/startXXcXX/{id}", name="matching_index_create_start")
     */
    public function CreateStart($id, Request $request)
    {
       // exit;
        $em = $this->getDoctrine()->getManager();
        $test = $request->get('RETEST');

        if ($test !== '') {
            $sql = "DELETE FROM  `utilisateur_profilmatching`  WHERE `idUser` =$id ";
            $em->getConnection()->query($sql);
        }

        $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =$id ";
        $res = $em->getConnection()->FetchAll($sql);
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_index_start', array('key' => $cle)));
        }
    }


    /**
     * @Route("/matching/start/{key}", name="matching_index_start")
     */
    public function Start($key, Request $request)
    {
        $this->CheckConnnection($request, $key);
        //var_dump($this->user);
        //var_dump($this->FullUser);
        //var_dump($this->ProfilMatching);

        if ($this->ProfilMatching == null) {
            return $this->redirect($this->generateUrl('autentification_fausse'));
        }

        if (strpos($this->ProfilMatching['route'],'Finish')>0) {
            $this->setValue('route', 'matching_Finish');
            return $this->redirect($this->generateUrl('matching_Finish'));
            } else {
            $this->setValue('route', 'matching_index');
            return $this->redirect($this->generateUrl('matching_index'));
            }
        }


    public function setValue($champs, $valeur)
    {
        $em = $this->getDoctrine()->getManager();
        $sql = "UPDATE utilisateur_profilmatching SET  `$champs`='$valeur' WHERE `idUser`=" . $this->user;
        $em->getConnection()->query($sql);
    }

    public function FullTest($CurrentRoute, Request $request)
    {
        $this->CheckConnnection($request, null);

        if ($this->ProfilMatching == null) {
            return $this->generateUrl('autentification_fausse');
        }

        if ($CurrentRoute !== $this->ProfilMatching['route']) {
            return $this->generateUrl($this->ProfilMatching['route']);
        }

        return null;
    }


    /**
     * @Route("/matching", name="matching_index")
     */
    public function index(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('objectif', $choix);

            if ($choix == 'poids') {
                $this->setValue('route', 'matching_Poids');
                return $this->redirect($this->generateUrl('matching_Poids'));
            }

            if ($choix == 'sante') {
                $this->setValue('route', 'matching_Sante_sports');
                return $this->redirect($this->generateUrl('matching_Sante_sports'));
            }

            if ($choix == 'perf') {
                $this->setValue('route', 'matching_Perfo_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Perfo_sports_pratique'));
            }

            if ($choix == 'muscle') {
                $this->setValue('route', 'matching_Perfo_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Perfo_sports_pratique'));
            }
        }

        return $this->render('matching/index.html.twig', [
        ]);
    }

    /**
     * @Route("/matching/Poids", name="matching_Poids")
     */
    public function Poids(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('kilos_a_perdre', $choix);

            $this->setValue('route', 'matching_Poids_sports');
            return $this->redirect($this->generateUrl('matching_Poids_sports'));
        }

        return $this->render('matching/poids.combien.twig', [
        ]);
    }


    /**
     * @Route("/matching/Sante/sport/", name="matching_Sante_sports")
     */
    public function SanteSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');

        if ($choix !== null) {
            if ($choix == 'Oui') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_Sante_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Sante_sports_pratique'));
            }

            if ($choix == 'Non') {
                $this->setValue('fait_du_sport', 0);
                $this->setValue('route', 'matching_Sante_sports_souhait');
                return $this->redirect($this->generateUrl('matching_Sante_sports_souhait'));
            }
        }

        return $this->render('matching/poids.sport.twig', [
            'type' => 'Sante'
        ]);
    }

    /**
     * @Route("/matching/Poids/sport/", name="matching_Poids_sports")
     */
    public function PoidsSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');

        if ($choix !== null) {

            if ($choix == 'Oui') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_Poids_sports_pratique');
                return $this->redirect($this->generateUrl('matching_Poids_sports_pratique'));
            }

            if ($choix == 'Non') {
                $this->setValue('fait_du_sport', 0);
                $this->setValue('route', 'matching_Poids_sports_souhait');
                return $this->redirect($this->generateUrl('matching_Poids_sports_souhait'));
            }


        }

        return $this->render('matching/poids.sport.twig', [
            'type' => 'Poids'

        ]);
    }

    /**
     * @Route("/matching/Sante/sport/souhait", name="matching_Sante_sports_souhait")
     * @Route("/matching/Poids/sport/souhait", name="matching_Poids_sports_souhait")
     */
    public function PoidsSportsSouhait(Request $request)
    {
        $choix = $request->get('choix');
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        if ($choix !== null) {

            if ($choix == 'Oui') {
                $this->setValue('sport_souhaite_faire', 1);

                if ($currentRoute == 'matching_Sante_sports_souhait') {

                    $this->setValue('route', 'matching_Sante_sports_pratique_Souhait');
                    return $this->redirect($this->generateUrl('matching_Sante_sports_pratique_Souhait'));
                } else {
                    $this->setValue('route', 'matching_Poids_sports_pratique_Souhait');
                    return $this->redirect($this->generateUrl('matching_Poids_sports_pratique_Souhait'));
                }
            }

            if ($choix == 'Non') {
                $this->setValue('sport_souhaite_faire', 0);
                $this->setValue('route', 'matching_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_RegimeAlimentaire'));
            }
        }

        return $this->render('matching/poids.sport.souhait.twig', [
            'currentRoute' => $currentRoute
        ]);
    }


    /**
     * @Route("/matching/Perfo/sport/pratique", name="matching_Perfo_sports_pratique")
     * @Route("/matching/Sante/sport/pratique", name="matching_Sante_sports_pratique")
     * @Route("/matching/Poids/sport/pratique", name="matching_Poids_sports_pratique")
     */
    public function PoidsPratiqueSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }
        $formBuilder = $this->createFormBuilder();

        $formBuilder
            ->add('sports', ChoiceType::class, array('required' => true, 'multiple' => true, 'expanded' => true, 'choices' => ['Course à pied' => 'Course à pied', 'CrossFit' => 'CrossFit', 'Musculation' => 'Musculation', 'Vélo' => 'vélo', 'Triathlon' => 'triathlon', 'Bodybuilding' => 'Bodybuilding', 'Autre' => 'Autre']))
            ->add('autre', TextareaType::class, array('required' => false, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Valider'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (($form->isSubmitted()) and ($form->isValid())) {
                if (!isset($_POST['form']['sports'])) {
                 goto suite;
                }

            $this->setValue('sport_pratique', addslashes(json_encode($_POST['form']['sports'])));
            $this->setValue('sport_pratique_autre', addslashes($_POST['form']['autre']));


            if ($currentRoute == 'matching_Perfo_sports_pratique') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_Perfo_sports_experience_pratique');
                return $this->redirect($this->generateUrl('matching_Perfo_sports_experience_pratique'));
            } else {
                $this->setValue('route', 'matching_Sante_sports_experience_pratique');
                return $this->redirect($this->generateUrl('matching_Sante_sports_experience_pratique'));
            }
        }


        suite:
        return $this->render('matching/sport.pratique.twig', [
            'form' => $form->createView(),
            'currentRoute' => $currentRoute
        ]);
    }


    /**
     * @Route("/matching/Perfo/sport/experience_pratique", name="matching_Perfo_sports_experience_pratique")
     * @Route("/matching/Sante/experience_pratique", name="matching_Sante_sports_experience_pratique")
     */
    public function PoidsPratiqueExperienceSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('Experience_sport', $choix);
            if ($currentRoute == 'matching_Perfo_sports_experience_pratique') {
                $this->setValue('route', 'matching_Perfo_sports_objectif');
                return $this->redirect($this->generateUrl('matching_Perfo_sports_objectif'));
            } else {
                    $this->setValue('route', 'matching_RegimeAlimentaire');
                    return $this->redirect($this->generateUrl('matching_RegimeAlimentaire'));
                    }

        }

        return $this->render('matching/sport.experience.twig', [
        ]);
    }


    /**
     * @Route("/matching/Perfo/sport/objectif", name="matching_Perfo_sports_objectif")
     */
    public function PerfSportObjectif(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('sport_objectif', $choix);

            if ($choix == 'competition') {
                $this->setValue('route', 'matching_Perfo_datecompetition');
                return $this->redirect($this->generateUrl('matching_Perfo_datecompetition'));
            }

            if ($choix == 'perf_sportive') {
                $this->setValue('route', 'matching_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_RegimeAlimentaire'));
            }

            if ($choix == 'esthetique') {
                $this->setValue('route', 'matching_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_RegimeAlimentaire'));
            }


        }

        return $this->render('matching/perf.objectif.twig', [
        ]);
    }


    /**
     * @Route("/matching/Perfo/sport/datecompetition", name="matching_Perfo_datecompetition")
     */
    public function PerfDateCompetition(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('delai_competition', $choix);

            $this->setValue('route', 'matching_RegimeAlimentaire');
            return $this->redirect($this->generateUrl('matching_RegimeAlimentaire'));

        }

        return $this->render('matching/perf.datecompetition.twig', [
        ]);
    }

    /**
     * @Route("/matching/Sante/sport/pratique_souhait", name="matching_Sante_sports_pratique_Souhait")
     * @Route("/matching/Poids/sport/pratique_souhait", name="matching_Poids_sports_pratique_Souhait")
     */
    public function PoidsPratiqueSportsSouhait(Request $request)
    {
        $formBuilder = $this->createFormBuilder();
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $formBuilder
            ->add('sports', ChoiceType::class, array('required' => true, 'multiple' => true, 'expanded' => true, 'choices' => ['Course à pied' => 'Course à pied', 'CrossFit' => 'CrossFit', 'Musculation' => 'Musculation', 'Vélo' => 'vélo', 'Triathlon' => 'triathlon', 'Bodybuilding' => 'Bodybuilding', 'Autre' => 'Autre']))
            ->add('autre', TextareaType::class, array('required' => false, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Valider'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (($form->isSubmitted()) and ($form->isValid())) {
            $this->setValue('sport_souhait', addslashes(json_encode($_POST['form']['sports'])));
            $this->setValue('sport_souhait_autre', addslashes($_POST['form']['autre']));

            $this->setValue('route', 'matching_RegimeAlimentaire');

            return $this->redirect($this->generateUrl('matching_RegimeAlimentaire')

            );
        }


        return $this->render('matching/poids.sport.pratique.souhait.twig', [
            'form' => $form->createView(),
            'currentRoute' => $currentRoute

        ]);
    }


    /**
     * @Route("/matching/Regime_Alimentaire", name="matching_RegimeAlimentaire")
     */
    public function RegimeAlimentaire(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('regime_alimentaire', $choix);
            $this->setValue('route', 'matching_HommeFemme');

            return $this->redirect($this->generateUrl('matching_HommeFemme'));
        }

        return $this->render('matching/RegimeAlimentaire.twig', [
            'currentRoute' => $currentRoute

        ]);
    }

    /**
     * @Route("/matching/HommeFemme", name="matching_HommeFemme")
     */
    public function HommeFemme(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('Homme_Femme', $choix);
            $this->setValue('route', 'matching_Finish');

            return $this->redirect($this->generateUrl('matching_Finish'));
        }

        return $this->render('matching/poids.HommeFemme.twig', [
        ]);
    }

    /**
     * @Route("/matching/Finish", name="matching_Finish")
     */
    public function Finish(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');
        return $this->render('matching/Finish.twig', [
        ]);
    }



    /**
     * @Route("/matching/RechercherCoach", name="matching_RechercherCoach")
     */
    public function Rechercher(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute , $request);

        $formBuilder = $this->createFormBuilder();
        $formBuilder
            ->add('name', TextType::class, array('required' => true, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Rechercher'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        if (($form->isSubmitted()) and ($form->isValid())) {
            $NOM=trim(addslashes($_POST['form']['name']));
            $sql="SELECT * FROM ccp.`admin` WHERE idAdmin<>14 AND `etat` = 1 AND (prenomadmin LIKE '%$NOM%' OR pseudoadmin LIKE '%$NOM%') LIMIT 5  ";
            $TabCoachFinal=$em->getConnection()->FetchAll($sql);
            $Recherche='1';
        } else {
            $Recherche='0';
            $TabCoachFinal=null;}

        return $this->render('matching/RechercherCoach.twig', [
            'Coachs' => $TabCoachFinal,
            'Recherche'=>$Recherche,
            'form' => $form->createView(),
            'Token' => $this->FullUser['token_clt']
        ]);
    }


    /**
     * @Route("/matching/Calcul", name="matching_Calcul")
     */
    public function Calcul(Request $request)
    {
        $debug = false;
        //$debug =true;

        $TabCoach = array();
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        $em = $this->getDoctrine()->getManager();

        $coaching = '1';
        $nutrition = '1';


        if ($request->get('RESET')=='1') {

            $this->setValue('route', 'matching_index');
            return $this->redirect($this->generateUrl('matching_index'));

            exit;
        }

        $sql = "SELECT REQ.* FROM (SELECT PR.*,100 AS SCORE,AD.etat FROM `coach_profilmatching` PR LEFT JOIN ccp.admin AD ON (AD.idAdmin=PR.idcoach) WHERE  `idcoach` > 0  ";

        if ($coaching == '1') {
            $sql .= ' AND coach_sport=1 ';
        }

        if ($nutrition == '1') {
            $sql .= ' AND coach_nutrition=1 ';
        }
        $sql .= ' ) as REQ WHERE REQ.etat=1';


        $objectif = $this->ProfilMatching['objectif'];
        $PreferenceSexe = $this->ProfilMatching['Homme_Femme'];
        $fait_du_sport = $this->ProfilMatching['fait_du_sport'];
        $sport_pratique = json_decode($this->ProfilMatching['sport_pratique']);
        $sport_souhaite_faire = $this->ProfilMatching['sport_souhaite_faire'];
        $sport_souhait = json_decode($this->ProfilMatching['sport_souhait']);

        $regime = $this->ProfilMatching['regime_alimentaire'];
        if ($regime == 'flexitarien') {
            $regime = 'généraliste';
        }

        if ($debug) {
            var_dump($objectif, $this->FullUser);
        }
        $Sexe = strtolower($this->FullUser['sexe']);
        $sport_objectif = $this->ProfilMatching['sport_objectif'];
        $Experience_sport= $this->ProfilMatching['Experience_sport'];

        $TestSportNutri = false;
        $TestSportPerf = false;
        if ($objectif == 'poids') {
            $OBJcoach = 'perte de poids ' . $Sexe;
            $TestSportNutri = true;
        }

        if ($objectif == 'sante') {
            $OBJcoach = 'recherche de bien-être';
            $TestSportNutri = true;
        }

        if ($objectif == 'perf') {
            $OBJcoach = 'préparation physique générale ' . $Sexe;
            $TestSportPerf = true;
        }

        if ($objectif == 'muscle') {
            $OBJcoach = 'prise de muscle ' . $Sexe;
            $TestSportPerf = true;
        }



        $Niveau = 'débutant';
        if ($Experience_sport>1) {$Niveau = 'intermédiaire';}
        if ($Experience_sport>3) {$Niveau = 'confirmé';}
        if (($objectif == 'perf') or ($objectif == 'muscle')) {
            if ($sport_objectif == 'competition') {
                $Niveau = 'compétition';
            }
        }

        $res = $em->getConnection()->FetchAll($sql);

        if ($debug) {
            var_dump($this->ProfilMatching);
            var_dump('----');
        }
        foreach ($res as $R) {
            $SCORE = $R['SCORE'];
            //Ponderation sur le fait qu'il propose du coaching en plus que ce qui est demandé...
            if (!($nutrition == $R['coach_nutrition'])) {
                $SCORE = $SCORE * 0.6;
            }

            if (!($coaching == $R['coach_sport'])) {
                $SCORE = $SCORE * 0.6;
            }


            if ($Niveau == 'compétition') {
                if (strpos($R['niveau'], $Niveau) !== false) {
                    if (strpos($R['objectifs'], $OBJcoach) !== false) {
                        $SCORE = $SCORE * 2;
                    } else {
                        $SCORE = 0;
                        goto fin;
                    }
                }
            }


            if (strpos($R['objectifs'], $OBJcoach) !== false) {
                $SCORE = $SCORE * sqrt(1 / $R['objectifs_nbr']);
            } else {
                $SCORE = 0;
                goto fin;

            }

            if ($nutrition == '1') {
                if (strpos($R['regime'], $regime) !== false) {
                    $SCORE = $SCORE * sqrt(1 / $R['regime_nbr']);
                } else {
                    $SCORE = $SCORE * 0.1;
                }
            }


            if ($objectif == 'poids') {
            }

            if ($objectif == 'sante') {
            }

            if ($fait_du_sport=='1') {

                if (strpos($R['niveau'], $Niveau) !== false) {
                    $SCORE = $SCORE * sqrt(1 / $R['niveau_nbr']);
                } else {
                    $SCORE = $SCORE * 0.2;
                }


                if ($TestSportNutri == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_nutritionnel'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_nutritionnel_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
                if ($TestSportPerf == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_sportif'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_sportif_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
            } elseif ($sport_souhaite_faire == '1') {
                if ($TestSportNutri == true) {
                    foreach ($sport_souhait as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_nutritionnel'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_nutritionnel_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
                if ($TestSportPerf == true) {
                    foreach ($sport_souhait as $sport) {
                        $sport=strtolower($sport);
                        if (strpos($R['sport_coaching_sportif'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_sportif_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
            }

            if ($PreferenceSexe !== 'les deux') {
                if (strtolower($PreferenceSexe) !== strtolower($R['sexe_coach'])) {
                    $SCORE = $SCORE * 0.5;
                }
            }


            //On recupere les Coachs qui correspondent aux crietes Primaire

            fin:
            $R['SCORE'] = $SCORE;
            if ($SCORE > 0) {
                $TabCoach[] = $R;
            }
            if ($debug) {
                var_dump($R);
            }
        }

        $sort = array_column($TabCoach, 'SCORE');
        array_multisort($sort, SORT_DESC, $TabCoach);
        if (sizeof($TabCoach)>0) {
            while ($TabCoach[0]['SCORE'] < 90) {
                for ($i = 0; $i < sizeof($TabCoach); $i++) {
                    $TabCoach[$i]['SCORE'] = $TabCoach[$i]['SCORE'] * 1.1;
                }
            }
        }
        if ($debug) {
            var_dump($TabCoach);
            }

        $SelCoach=-1;
        $TabCoachFinal = array();
        $Nb = min(sizeof($TabCoach), 3);
        for ($i = 0; $i < $Nb; $i++) {
            $sql = "SELECT *,'" . $TabCoach[$i]['SCORE'] . "' AS SCORE FROM ccp.`admin` WHERE `idAdmin`=" . $TabCoach[$i]['idcoach'];
            $res2 = $em->getConnection()->FetchAll($sql);

            $TabCoachFinal[] =  $res2[0];
            $TabCoachFinal[$i]['profil'] =  $TabCoach[$i];

            if ($request->get('cidc')==$TabCoach[$i]['idcoach']) {
                $SelCoach=$i;
            }
        }

//        var_dump($TabCoachFinal);
        if ( $SelCoach<0 and $Nb>0) {
            $SelCoach=0;
        }

        //     var_dump($SelCoach);

        //   var_dump($this->FullUser['token_clt']);
        return $this->render('matching/Coachs.twig', [
            'SelCoach'=>$SelCoach,
            'Coachs' => $TabCoachFinal,
            'Token' =>$this->FullUser['token_clt']
        ]);
    }
}
