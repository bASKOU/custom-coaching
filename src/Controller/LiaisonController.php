<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class LiaisonController extends AbstractController
{
    /**
     * @Route("/in/url1", name="url_1")
     * @Route("/in/url2", name="url_2")
     * @Route("/in/url3", name="url_3")
     */
    public function url1(Request $request)
    {
        $token = $request->get('token');

        if ($token=='') {
            return $this->redirect($this->generateUrl('index'));
            }
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM ccp.`utilisateur`  WHERE etatUser=1 AND  `token_clt` ='$token'";
            $res = $em->getConnection()->FetchAll($sql);
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_index_start', array('key' => $cle)));
            }

        return $this->redirect($this->generateUrl('index'));

        //Check si

    }

    /**
     * @Route("/in/lp1", name="url_lp1")
     */
    public function lp1(Request $request)
    {
        $token = $request->get('token');

        if ($token=='') {
            return $this->redirect($this->generateUrl('index'));
            }


        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM ccp.`utilisateur` WHERE etatUser=1 AND `token_clt` ='$token'";
        $res = $em->getConnection()->FetchAll($sql);
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_Lp1_index_start', array('key' => $cle)));
        }

        return $this->redirect($this->generateUrl('index'));

        //Check si

    }


    /**
     * @Route("/in/ToutEstPossibleStartMatching", name="url_ToutEstPossible1_StartMatching")
     */
    public function lpToutEstPossibleStartMatching(Request $request)
    {
        $token = $request->get('token');

        if ($token=='') {
            return $this->redirect($this->generateUrl('index'));
                }

        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM ccp.`utilisateur` WHERE  etatUser=1 AND `token_clt` ='$token'";
        $res = $em->getConnection()->FetchAll($sql);
        //var_dump($res);
        //exit;
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_ToutEstPossible_index_start', array('key' => $cle)));
            }

        return $this->redirect($this->generateUrl('index'));

        //Check si

    }

    /**
     * @Route("/in/ToutEstPossible1", name="url_ToutEstPossible11")
     */
    public function ToutEstPossible1_1(Request $request)
        {
            //demoloc.custom-coaching.fr/in/ToutEstPossible1?mail=damienguigue@gmail.com

        $mail = $request->get('mail');

        if ($mail=='') {
            return $this->redirect($this->generateUrl('index'));
        }
        /*
         * On Verifie Mail est bien TOUTESTPOSSIBLE
         *
         */
            $em = $this->getDoctrine()->getManager();
            $sql = "SELECT * FROM `tep3` WHERE mail='$mail'";
            $res = $em->getConnection()->FetchAll($sql);
            if (!isset($res[0]['etat'])) {
                return $this->redirect($this->generateUrl('index'));
                }


            if (!($res[0]['etat']=='attente')) {
               echo 'Vous avez déjà profité de cette offre.';
               exit;
               }

       $session = new Session();
       $session->start();
       $session->set('MailToutestpossible',$mail);


        return $this->redirect($this->generateUrl('matching_ToutEstPossible_start'));
        //Check si
    }


    /**
     * @Route("/in/lp1RDV", name="url_lp1RDV")
     */
    public function lp1RDV(Request $request)
    {
    $token = $request->get('token');

        if ($token=='') {
            return $this->redirect($this->generateUrl('index'));
        }
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM ccp.`utilisateur` WHERE etatUser=1 AND `token_clt` ='$token'";
        $res = $em->getConnection()->FetchAll($sql);
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_Lp1_rdvredirect', array('key' => $cle)));
        }

        return $this->redirect($this->generateUrl('index'));
        //Check si
    }
    /**
     * @Route("/", name="index")
     */
    public function index()
    {

        return $this->render('erreur.twig', [
        ]);
    }
}
