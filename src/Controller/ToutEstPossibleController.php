<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class ToutEstPossibleController extends AbstractController
{
    const CALENDY = "https://calendly.com/natacha-bonnal/customcoaching";
    private $user;
    private $FullUser;
    private $ProfilMatching;

    private function CheckConnnection($request, $Key)
    {
        $CurrentClid = null;
        $clidTest = null;
        $em = $this->getDoctrine()->getManager();
        if ($Key !== null) {
            $TabC = explode('_', $Key);
            $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` ='" . $TabC[0] . "'";
            $res = $em->getConnection()->FetchAll($sql);
            if (isset($res[0]['motdepassUser'])) {
                $clidTest = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            }

            if ($clidTest == $Key) {
                $CurrentClid = $clidTest;
                $duree = time() + 3600 * 24 * 180;
                $cookie = new Cookie('CL', $CurrentClid, $duree);
                $response = new Response();
                $response->headers->setCookie($cookie);
                $response->send();
            }
        } else {
            $cookies = $request->cookies;
            if ($cookies->has('CL')) {
                $TabC = explode('_', $cookies->get('CL'));
                $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =" . $TabC[0];
                $res = $em->getConnection()->FetchAll($sql);
                if (isset($res[0]['motdepassUser'])) {
                    $clidTest = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
                    if ($clidTest == $cookies->get('CL')) {
                        $CurrentClid = $clidTest;
                    }
                }
            }
        }

        if ($CurrentClid !== null) {
            $TabC = explode('_', $CurrentClid);
            // var_dump($TabC);
            $this->user = $TabC[0];
            $this->FullUser = $res[0];

            test:
            $sql = "SELECT * FROM `utilisateur_profilmatching` WHERE `idUser` =" . $TabC[0];
            $res = $em->getConnection()->FetchAll($sql);
            if ($res == null) {
                $sql = "INSERT INTO `utilisateur_profilmatching` (`IdUser`, `route`) VALUES ('" . $TabC[0] . "', 'matching_ToutEstPossible_index');";
                $em->getConnection()->query($sql);
                goto test;
            }
            $this->ProfilMatching = $res[0];

        }
    }

    /**
     * @Route("OFFF/ToutEstPossible/create/startXXcXX/{id}", name="matching_ToutEstPossible_index_create_start")
     */
    public function CreateStart($id, Request $request)
    {
        exit;
        $em = $this->getDoctrine()->getManager();
        $test = $request->get('RETEST');

        if ($test !== '') {
            $sql = "DELETE FROM  `utilisateur_profilmatching`  WHERE `idUser` =$id ";
            $em->getConnection()->query($sql);
        }

        $sql = "SELECT * FROM ccp.`utilisateur` WHERE `idUser` =$id ";
        $res = $em->getConnection()->FetchAll($sql);
        if (isset($res[0]['motdepassUser'])) {
            //var_dump($res[0]);
            $cle = $res[0]['idUser'] . '_' . md5('KEY_AlgoE495j5EEDFDF' . $res[0]['motdepassUser'] . '-' . $res[0]['idUser']);
            return $this->redirect($this->generateUrl('matching_ToutEstPossible_index_start', array('key' => $cle)));
        }
    }


    /**
     * @Route("/ToutEstPossible/start/{key}", name="matching_ToutEstPossible_index_start")
     */
    public function Start($key, Request $request)
    {
        $this->CheckConnnection($request, $key);
        //var_dump($this->user);
        //var_dump($this->FullUser);
        //var_dump($this->ProfilMatching);

        if ($this->ProfilMatching == null) {
            return $this->redirect($this->generateUrl('autentification_fausse'));
        }
        return $this->redirect($this->generateUrl($this->ProfilMatching['route']));
    }

    /**
     * @Route("/ToutEstPossible/rdv/{key}", name="matching_ToutEstPossible_rdvredirect")
     */
    public function RdvRedirect($key, Request $request)
    {
        $this->CheckConnnection($request, $key);
        //var_dump($this->user);
        //var_dump($this->FullUser);
        //var_dump($this->ProfilMatching);

        if ($this->ProfilMatching == null) {
            return $this->redirect($this->generateUrl('autentification_fausse'));
        }
        return $this->redirect($this->generateUrl('matching_ToutEstPossible_Redirect'));
    }


    public function setValue($champs, $valeur)
    {
        $em = $this->getDoctrine()->getManager();
        $sql = "UPDATE utilisateur_profilmatching SET  `$champs`='$valeur' WHERE `idUser`=" . $this->user;
        $em->getConnection()->query($sql);
    }

    public function FullTest($CurrentRoute, Request $request)
    {
        $this->CheckConnnection($request, null);

        if ($this->ProfilMatching == null) {
            return $this->generateUrl('autentification_fausse');
        }

        if ($CurrentRoute !== $this->ProfilMatching['route']) {
            return $this->generateUrl($this->ProfilMatching['route']);
        }

        return null;
    }

    public function SimpleTest(Request $request)
    {
        $this->CheckConnnection($request, null);

        if ($this->ProfilMatching == null) {
            return $this->generateUrl('autentification_fausse');
        }


        return null;
    }

    /**
     * @Route("/ToutEstPossibe/Exist", name="matching_ToutEstPossible_startExist")
     */
    public function index(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('objectif', $choix);

            if ($choix == 'commencer') {
                $this->setValue('route', 'matching_ToutEstPossible_index');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_index'));
            }


            if ($choix == 'rdv') {
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Redirect'));

            }
        }

        return $this->render('landing_page1/index.html.twig', [
                'mail' => $this->FullUser['emailUser']]
        );
    }


    /**
     * @Route("/ToutEstPossibe/", name="matching_ToutEstPossible_start")
     */
    public function indexNew(Request $request)
    {
        $session = new Session();
        $session->start();

        // set and get session attributes

        $choix = $request->get('choix');

        if ($choix !== null) {
            if ($choix == 'commencer') {
                $mail = $session->get('MailToutestpossible');


                $em = $this->getDoctrine()->getManager();
                $sql = "SELECT `idUser`, `token_clt` FROM ccp.`utilisateur` WHERE etatUser=1 AND `emailUser`='$mail'  ";
                $res = $em->getConnection()->FetchAll($sql);
                if (isset($res[0]['idUser'])) {
                    $sql = "DELETE FROM  `utilisateur_profilmatching`  WHERE `idUser` =".$res[0]['idUser'];
                    $em->getConnection()->query($sql);

                    return $this->redirect($this->generateUrl('url_ToutEstPossible1_StartMatching').'?token='.$res[0]['token_clt']);
                    //exit;
                }


                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Creation'));
            }


            if ($choix == 'rdv') {
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Redirect'));

            }
        }

        return $this->render('tout_est_possible/index.html.twig', [
                'mail' => $this->FullUser['emailUser']]
        );
    }


    /**
     * @Route("/ToutEstPossibe/Creation_Compte", name="matching_ToutEstPossible_Creation")
     */
    public function indexCreationCompte(Request $request)
    {

        $session = new Session();
        $session->start();

        // set and get session attributes
        $mail = $session->get('MailToutestpossible');
        //var_dump($mail);
        //exit;

        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM `tep3` WHERE mail='$mail'";
        $res = $em->getConnection()->FetchAll($sql);
        if (!isset($res[0]['etat'])) {
            return $this->redirect($this->generateUrl('index'));
        }


        if (!($res[0]['etat'] == 'attente')) {
            echo 'Vous avez déjà profité de cette offre.';
            exit;
        }


        return $this->render('tout_est_possible/CreationCompte.twig', [
            'mail' => $mail
        ]);
    }

    /**
     * @Route("/ToutEstPossibe/Creation_Compte2", name="matching_ToutEstPossible_Creation2")
     */
    public function indexCreationCompte2(Request $request)
    {

        $session = new Session();
        $session->start();

        // set and get session attributes
        $mail = $session->get('MailToutestpossible');

        $token = $request->get('token');
        $url_api = $_ENV['url_api'];
        //var_dump($token);
        //exit;
        /*
*/
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url_api . 'info_user?token=' . $token,
            CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        $user = json_decode($resp, true);

        //var_dump($user['idUser']);
        //exit;

        return $this->render('tout_est_possible/CreationCompte2.twig', [
            'mail' => $mail,
            'token' => $token,
            'idUser' => $user['idUser']
        ]);
    }


    /**
     * @Route("/ToutEstPossible_index/", name="matching_ToutEstPossible_index")
     */
    public function Landing(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('objectif', $choix);

            if ($choix == 'poids') {
                $this->setValue('route', 'matching_ToutEstPossible_Poids');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Poids'));
            }

            if ($choix == 'sante') {
                $this->setValue('route', 'matching_ToutEstPossible_Sante_sports');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Sante_sports'));
            }

            if ($choix == 'perf') {
                $this->setValue('route', 'matching_ToutEstPossible_Perfo_sports_pratique');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Perfo_sports_pratique'));
            }

            if ($choix == 'muscle') {
                $this->setValue('route', 'matching_ToutEstPossible_Perfo_sports_pratique');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Perfo_sports_pratique'));
            }
        }

        return $this->render('matching/index.html.twig', [
                'mail' => $this->FullUser['emailUser']]
        );
    }

    /**
     * @Route("/ToutEstPossible/Poids", name="matching_ToutEstPossible_Poids")
     */
    public function Poids(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('kilos_a_perdre', $choix);

            $this->setValue('route', 'matching_ToutEstPossible_Poids_sports');
            return $this->redirect($this->generateUrl('matching_ToutEstPossible_Poids_sports'));
        }

        return $this->render('matching/poids.combien.twig', [
            'mail' => $this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/ToutEstPossible/Sante/sport/", name="matching_ToutEstPossible_Sante_sports")
     */
    public function SanteSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');

        if ($choix !== null) {
            if ($choix == 'Oui') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_ToutEstPossible_Sante_sports_pratique');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Sante_sports_pratique'));
            }

            if ($choix == 'Non') {
                $this->setValue('fait_du_sport', 0);
                $this->setValue('route', 'matching_ToutEstPossible_Sante_sports_souhait');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Sante_sports_souhait'));
            }
        }

        return $this->render('matching/poids.sport.twig', [
            'type' => 'Sante'
            , 'mail' => $this->FullUser['emailUser']

        ]);
    }

    /**
     * @Route("/ToutEstPossible/Poids/sport/", name="matching_ToutEstPossible_Poids_sports")
     */
    public function PoidsSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');

        if ($choix !== null) {

            if ($choix == 'Oui') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_ToutEstPossible_Poids_sports_pratique');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Poids_sports_pratique'));
            }

            if ($choix == 'Non') {
                $this->setValue('fait_du_sport', 0);
                $this->setValue('route', 'matching_ToutEstPossible_Poids_sports_souhait');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Poids_sports_souhait'));
            }


        }

        return $this->render('matching/poids.sport.twig', [
            'type' => 'Poids',
            'mail' => $this->FullUser['emailUser']


        ]);
    }

    /**
     * @Route("/ToutEstPossible/Sante/sport/souhait", name="matching_ToutEstPossible_Sante_sports_souhait")
     * @Route("/ToutEstPossible/Poids/sport/souhait", name="matching_ToutEstPossible_Poids_sports_souhait")
     */
    public function PoidsSportsSouhait(Request $request)
    {
        $choix = $request->get('choix');
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        if ($choix !== null) {

            if ($choix == 'Oui') {
                $this->setValue('sport_souhaite_faire', 1);

                if ($currentRoute == 'matching_ToutEstPossible_Sante_sports_souhait') {

                    $this->setValue('route', 'matching_ToutEstPossible_Sante_sports_pratique_Souhait');
                    return $this->redirect($this->generateUrl('matching_ToutEstPossible_Sante_sports_pratique_Souhait'));
                } else {
                    $this->setValue('route', 'matching_ToutEstPossible_Poids_sports_pratique_Souhait');
                    return $this->redirect($this->generateUrl('matching_ToutEstPossible_Poids_sports_pratique_Souhait'));
                }
            }

            if ($choix == 'Non') {
                $this->setValue('sport_souhaite_faire', 0);
                $this->setValue('route', 'matching_ToutEstPossible_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_RegimeAlimentaire'));
            }
        }

        return $this->render('matching/poids.sport.souhait.twig', [
            'currentRoute' => $currentRoute,
            'mail' => $this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/ToutEstPossible/Perfo/sport/pratique", name="matching_ToutEstPossible_Perfo_sports_pratique")
     * @Route("/ToutEstPossible/Sante/sport/pratique", name="matching_ToutEstPossible_Sante_sports_pratique")
     * @Route("/ToutEstPossible/Poids/sport/pratique", name="matching_ToutEstPossible_Poids_sports_pratique")
     */
    public function PoidsPratiqueSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }
        $formBuilder = $this->createFormBuilder();

        $formBuilder
            ->add('sports', ChoiceType::class, array('required' => true, 'multiple' => true, 'expanded' => true, 'choices' => ['Course à pied' => 'Course à pied', 'CrossFit' => 'CrossFit', 'Musculation' => 'Musculation', 'Vélo' => 'vélo', 'Triathlon' => 'triathlon', 'Bodybuilding' => 'Bodybuilding', 'Autre' => 'Autre']))
            ->add('autre', TextareaType::class, array('required' => false, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Valider'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (($form->isSubmitted()) and ($form->isValid())) {
            if (!isset($_POST['form']['sports'])) {
                goto suite;
            }

            $this->setValue('sport_pratique', addslashes(json_encode($_POST['form']['sports'])));
            $this->setValue('sport_pratique_autre', addslashes($_POST['form']['autre']));


            if ($currentRoute == 'matching_ToutEstPossible_Perfo_sports_pratique') {
                $this->setValue('fait_du_sport', 1);
                $this->setValue('route', 'matching_ToutEstPossible_Perfo_sports_experience_pratique');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Perfo_sports_experience_pratique'));
            } else {
                $this->setValue('route', 'matching_ToutEstPossible_Sante_sports_experience_pratique');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Sante_sports_experience_pratique'));
            }
        }


        suite:
        return $this->render('matching/sport.pratique.twig', [
            'form' => $form->createView(),
            'currentRoute' => $currentRoute,
            'mail' => $this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/ToutEstPossible/Perfo/sport/experience_pratique", name="matching_ToutEstPossible_Perfo_sports_experience_pratique")
     * @Route("/ToutEstPossible/Sante/experience_pratique", name="matching_ToutEstPossible_Sante_sports_experience_pratique")
     */
    public function PoidsPratiqueExperienceSports(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('Experience_sport', $choix);
            if ($currentRoute == 'matching_ToutEstPossible_Perfo_sports_experience_pratique') {
                $this->setValue('route', 'matching_ToutEstPossible_Perfo_sports_objectif');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Perfo_sports_objectif'));
            } else {
                $this->setValue('route', 'matching_ToutEstPossible_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_RegimeAlimentaire'));
            }

        }

        return $this->render('matching/sport.experience.twig', [
            'mail' => $this->FullUser['emailUser']

        ]);
    }


    /**
     * @Route("/ToutEstPossible/Perfo/sport/objectif", name="matching_ToutEstPossible_Perfo_sports_objectif")
     */
    public function PerfSportObjectif(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('sport_objectif', $choix);

            if ($choix == 'competition') {
                $this->setValue('route', 'matching_ToutEstPossible_Perfo_datecompetition');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Perfo_datecompetition'));
            }

            if ($choix == 'perf_sportive') {
                $this->setValue('route', 'matching_ToutEstPossible_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_RegimeAlimentaire'));
            }

            if ($choix == 'esthetique') {
                $this->setValue('route', 'matching_ToutEstPossible_RegimeAlimentaire');
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_RegimeAlimentaire'));
            }


        }

        return $this->render('matching/perf.objectif.twig', [
            'mail' => $this->FullUser['emailUser']
        ]);
    }


    /**
     * @Route("/ToutEstPossible/Perfo/sport/datecompetition", name="matching_ToutEstPossible_Perfo_datecompetition")
     */
    public function PerfDateCompetition(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('delai_competition', $choix);

            $this->setValue('route', 'matching_ToutEstPossible_RegimeAlimentaire');
            return $this->redirect($this->generateUrl('matching_ToutEstPossible_RegimeAlimentaire'));

        }

        return $this->render('matching/perf.datecompetition.twig', [
            'mail' => $this->FullUser['emailUser']
        ]);
    }

    /**
     * @Route("/ToutEstPossible/Sante/sport/pratique_souhait", name="matching_ToutEstPossible_Sante_sports_pratique_Souhait")
     * @Route("/ToutEstPossible/Poids/sport/pratique_souhait", name="matching_ToutEstPossible_Poids_sports_pratique_Souhait")
     */
    public function PoidsPratiqueSportsSouhait(Request $request)
    {
        $formBuilder = $this->createFormBuilder();
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $formBuilder
            ->add('sports', ChoiceType::class, array('required' => true, 'multiple' => true, 'expanded' => true, 'choices' => ['Course à pied' => 'Course à pied', 'CrossFit' => 'CrossFit', 'Musculation' => 'Musculation', 'Vélo' => 'vélo', 'Triathlon' => 'triathlon', 'Bodybuilding' => 'Bodybuilding', 'Autre' => 'Autre']))
            ->add('autre', TextareaType::class, array('required' => false, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Valider'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if (($form->isSubmitted()) and ($form->isValid())) {
            $this->setValue('sport_souhait', addslashes(json_encode($_POST['form']['sports'])));
            $this->setValue('sport_souhait_autre', addslashes($_POST['form']['autre']));

            $this->setValue('route', 'matching_ToutEstPossible_RegimeAlimentaire');

            return $this->redirect($this->generateUrl('matching_ToutEstPossible_RegimeAlimentaire')

            );
        }


        return $this->render('matching/poids.sport.pratique.souhait.twig', [
            'form' => $form->createView(),
            'currentRoute' => $currentRoute,
            'mail' => $this->FullUser['emailUser']

        ]);
    }


    /**
     * @Route("/ToutEstPossible/Regime_Alimentaire", name="matching_ToutEstPossible_RegimeAlimentaire")
     */
    public function RegimeAlimentaire(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('regime_alimentaire', $choix);
            $this->setValue('route', 'matching_ToutEstPossible_HommeFemme');

            return $this->redirect($this->generateUrl('matching_ToutEstPossible_HommeFemme'));
        }

        return $this->render('matching/RegimeAlimentaire.twig', [
            'currentRoute' => $currentRoute,
            'mail' => $this->FullUser['emailUser']


        ]);
    }

    /**
     * @Route("/ToutEstPossible/HommeFemme", name="matching_ToutEstPossible_HommeFemme")
     */
    public function HommeFemme(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        $choix = $request->get('choix');

        if ($choix !== null) {
            $this->setValue('Homme_Femme', $choix);
            $this->setValue('route', 'matching_ToutEstPossible_Finish');

            return $this->redirect($this->generateUrl('matching_ToutEstPossible_Finish'));
        }

        return $this->render('matching/poids.HommeFemme.twig', [
            'mail' => $this->FullUser['emailUser']

        ]);
    }

    /**
     * @Route("/ToutEstPossible/Finish", name="matching_ToutEstPossible_Finish")
     */
    public function Finish(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        if ($test !== null) {
            return $this->redirect($test);
        }


        $choix = $request->get('choix');
        return $this->render('tout_est_possible/Finish.twig', [
            'mail' => $this->FullUser['emailUser']

        ]);
    }

    /**
     * @Route("/ToutEstPossible/Merci", name="matching_ToutEstPossible_Merci")
     */
    public function Merci(Request $request)
    {
        return $this->render('landing_page1/Merci.twig', ['Lien' => self::CALENDY]);
    }


    /**
     * @Route("/ToutEstPossible/Redirect", name="matching_ToutEstPossible_Redirect")
     */
    public function RedirectLp(Request $request)
    {
        return $this->render('landing_page1/Redirect.twig', ['Lien' => self::CALENDY]);
    }

    /**
     * @Route("/ToutEstPossible/Felicitation", name="matching_ToutEstPossible_Felicitation")
     */
    public function OffreValide(Request $request)
    {

    }

    /**
     * @Route("/ToutEstPossible/ValidationOffre", name="matching_ToutEstPossible_ValidationOffre")
     */
    public function ValidationOffre(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $debug = false;
        //$debug =true;
        $url_api = $_ENV['url_api'];
        $this->CheckConnnection($request, null);
        $mail = $this->FullUser['emailUser'];


        $validate = $request->get('validate');
        $coach = $request->get('coach');

        $sql = "SELECT * FROM `tep3` WHERE mail='$mail'";
        $res = $em->getConnection()->FetchAll($sql);
        if (!isset($res[0]['etat'])) {
            return $this->redirect($this->generateUrl('index'));
        }


        if (!($res[0]['etat'] == 'attente')) {
            echo 'Vous avez déjà profité de cette offre.';
            exit;
        }

        //On Valide Le Choix
        if (strlen($validate) > 0) {
            if (($validate == '1') or ($validate == '2')) {
                $url = $url_api . 'inserer_offre?offre=1&token=' . $this->FullUser['token_clt'] . '&idc=' . $coach . '&type_plan=' . $validate;
            } else
                {
                    $url = $url_api . 'inserer_offre?offre=1&token=' . $this->FullUser['token_clt'] . '&idc=' . $coach . '&type_plan=1' ;
                }

            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'Codular Sample cURL Request'
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            //var_dump($resp);
            $reponse = json_decode($resp, true);
            //var_dump($reponse);
            //var_dump($resp);
            //var_dump($url);

            if ($resp == '200') {
                $sql = "UPDATE `tep3` SET etat='utilise' WHERE mail='$mail'";
                $em->getConnection()->query($sql);
                if ($validate == '3')  {
                    return $this->redirect($this->generateUrl('matching_ToutEstPossible_RedirectPaiement'));
                }
                return $this->redirect($this->generateUrl('matching_ToutEstPossible_Felicitation'));


            }   else {
                Echo 'Erreur Inconnue. Merci de rafrachir la page';
                exit;

                }
            }

        $currentRoute = $request->attributes->get('_route');
        $IdCoach = $request->get('idc');
        $test = $this->FullTest($currentRoute, $request);
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT * FROM ccp.`admin` WHERE `idAdmin`=" . $IdCoach;
        $res2 = $em->getConnection()->FetchAll($sql);
        $Coach = $res2[0];


        //   var_dump($this->FullUser['token_clt']);
        return $this->render('tout_est_possible/Acheter.twig', [
            'SelCoach' => $IdCoach,
            'Coach' => $Coach,
            'Token' => $this->FullUser['token_clt']
        ]);

    }


    /**
     * @Route("/ToutEstPossible/Felicitation", name="matching_ToutEstPossible_Felicitation")
     */
    public function Felicitation(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $test = $this->SimpleTest( $request);
        if ($test !== null) {
            return $this->redirect($test);
            }

        return $this->render('tout_est_possible/Fin1.twig', [
        ]);

    }

    /**
     * @Route("/ToutEstPossible/RedirectPaiement", name="matching_ToutEstPossible_RedirectPaiement")
     */
    public function RedirectPaiement(Request $request)
    {
        $currentRoute = $request->attributes->get('_route');
        $em = $this->getDoctrine()->getManager();

        $test = $this->SimpleTest($request);
        if ($test !== null) {
            return $this->redirect($test);
        }

        //var_dump($this->user);
        //var_dump($this->FullUser);
        //RecupereCoach
        $sql = "SELECT * FROM ccp.`utilisateur_abonnement` WHERE `idUser` =" . $this->user . " AND `etat` = 1 ORDER BY `utilisateur_abonnement`.`id_utilisateur_abonnement` DESC ";
        $res = $em->getConnection()->FetchAll($sql);
        $IdCoach = $res[0]['id_coach'];
        $date_fin = $res[0]['date_fin'];
        $id_utilisateur_abonnement = $res[0]['id_utilisateur_abonnement'];
        return $this->render('tout_est_possible/Redirect.twig', [
            'IdCoach' => $IdCoach,
            'date_fin' => $date_fin,
            'User' => $this->FullUser,
            'id_utilisateur_abonnement' => $id_utilisateur_abonnement
        ]);
    }


    /**
     * @Route("/ToutEstPossible/RechercherCoach", name="matching_ToutEstPossible_RechercherCoach")
     */
    public function Rechercher(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $formBuilder = $this->createFormBuilder();
        $formBuilder
            ->add('name', TextType::class, array('required' => true, 'label' => ''))
            ->add('submit', SubmitType::class, array('label' => 'Rechercher'));

        $form = $formBuilder->getForm();
        $form->handleRequest($request);
        if (($form->isSubmitted()) and ($form->isValid())) {
            $NOM=trim(addslashes($_POST['form']['name']));
            $sql="SELECT * FROM ccp.`admin` WHERE idAdmin<>14 AND `etat` = 1 AND (prenomadmin LIKE '%$NOM%' OR pseudoadmin LIKE '%$NOM%') LIMIT 5  ";
            $TabCoachFinal=$em->getConnection()->FetchAll($sql);
            $Recherche='1';
            } else {
            $Recherche='0';
            $TabCoachFinal=null;
            }



        return $this->render('tout_est_possible/RechercherCoach.twig', [
            'Coachs' => $TabCoachFinal,
            'Recherche'=>$Recherche,
            'form' => $form->createView(),
            'Token' => $this->FullUser['token_clt']
        ]);

    }
    /**
     * @Route("/ToutEstPossible/Calcul", name="matching_ToutEstPossible_Calcul")
     */
    public function Calcul(Request $request)
    {
        $debug = false;
       // $debug =true;

        $TabCoach = array();
        $currentRoute = $request->attributes->get('_route');
        $test = $this->FullTest($currentRoute, $request);
        $em = $this->getDoctrine()->getManager();

        if ($request->get('RESET')=='1') {

            $sql = "DELETE FROM  `utilisateur_profilmatching`  WHERE `idUser` =".$this->user;
            $em->getConnection()->query($sql);
            //Echo 'Redirect SUR compte existe! ';
            return $this->redirect($this->generateUrl('url_ToutEstPossible1_StartMatching').'?token='.$this->FullUser['token_clt']);

            exit;
            }

        $coaching = '1';
        $nutrition = '1';

        $sql = "SELECT REQ.* FROM (SELECT PR.*,100 AS SCORE,AD.etat FROM `coach_profilmatching` PR LEFT JOIN ccp.admin AD ON (AD.idAdmin=PR.idcoach) WHERE  `idcoach` > 0  ";
        if ($coaching == '1') {
            $sql .= ' AND coach_sport=1 ';
        }

        if ($nutrition == '1') {
            $sql .= ' AND coach_nutrition=1 ';
        }
        $sql .= ' ) as REQ WHERE REQ.etat=1';

        $objectif = $this->ProfilMatching['objectif'];
        $PreferenceSexe = $this->ProfilMatching['Homme_Femme'];
        $fait_du_sport = $this->ProfilMatching['fait_du_sport'];
        $sport_pratique = json_decode($this->ProfilMatching['sport_pratique']);
        $sport_souhaite_faire = $this->ProfilMatching['sport_souhaite_faire'];
        $sport_souhait = json_decode($this->ProfilMatching['sport_souhait']);

        $regime = $this->ProfilMatching['regime_alimentaire'];
        if ($regime == 'flexitarien') {
            $regime = 'généraliste';
        }

        if ($debug) {
            var_dump($objectif, $this->FullUser);
        }
        $Sexe = strtolower($this->FullUser['sexe']);
        $sport_objectif = $this->ProfilMatching['sport_objectif'];
        $Experience_sport = $this->ProfilMatching['Experience_sport'];

        $TestSportNutri = false;
        $TestSportPerf = false;
        if ($objectif == 'poids') {
            $OBJcoach = 'perte de poids ' . $Sexe;
            $TestSportNutri = true;
        }

        if ($objectif == 'sante') {
            $OBJcoach = 'recherche de bien-être';
            $TestSportNutri = true;
        }

        if ($objectif == 'perf') {
            $OBJcoach = 'préparation physique générale ' . $Sexe;
            $TestSportPerf = true;
        }

        if ($objectif == 'muscle') {
            $OBJcoach = 'prise de muscle ' . $Sexe;
            $TestSportPerf = true;
        }


        $Niveau = 'débutant';
        if ($Experience_sport > 1) {
            $Niveau = 'intermédiaire';
        }
        if ($Experience_sport > 3) {
            $Niveau = 'confirmé';
        }
        if (($objectif == 'perf') or ($objectif == 'muscle')) {
            if ($sport_objectif == 'competition') {
                $Niveau = 'compétition';
            }
        }

        $res = $em->getConnection()->FetchAll($sql);

        if ($debug) {
            var_dump($this->ProfilMatching);
            var_dump('----');
        }
        foreach ($res as $R) {
            $SCORE = $R['SCORE'];
            //On Recherche le Nbr De client
            $sql='SELECT Count(*) ASNB,id_coach FROM ccp.`utilisateur_abonnement` WHERE `date_fin` > NOW() AND etat=1 AND  id_coach='.$R['idcoach'];
            $RCOACH=$em->getConnection()->FetchAll($sql);
            if ($RCOACH[0]['ASNB']<5) {
                $SCORE =$SCORE*5;
            } elseif ($RCOACH[0]['ASNB']<10) {
                $SCORE =$SCORE*3;
            }
             elseif ($RCOACH[0]['ASNB']<20) {
        $SCORE =$SCORE*2;
        }


            //Ponderation sur le fait qu'il propose du coaching en plus que ce qui est demandé...
            if (!($nutrition == $R['coach_nutrition'])) {
                $SCORE = $SCORE * 0.6;
            }

            if (!($coaching == $R['coach_sport'])) {
                $SCORE = $SCORE * 0.6;
            }


            if ($Niveau == 'compétition') {
                if (strpos($R['niveau'], $Niveau) !== false) {
                    if (strpos($R['objectifs'], $OBJcoach) !== false) {
                        $SCORE = $SCORE * 2;
                    } else {
                        $SCORE = 0;
                        goto fin;
                    }
                }
            }


            if (strpos($R['objectifs'], $OBJcoach) !== false) {
                $SCORE = $SCORE * sqrt(1 / $R['objectifs_nbr']);
            } else {
                $SCORE = 0;
                goto fin;

            }

            if ($nutrition == '1') {
                if (strpos($R['regime'], $regime) !== false) {
                    $SCORE = $SCORE * sqrt(1 / $R['regime_nbr']);
                } else {
                    $SCORE = $SCORE * 0.1;
                }
            }


            if ($objectif == 'poids') {
            }

            if ($objectif == 'sante') {
            }

            if ($fait_du_sport == '1') {

                if (strpos($R['niveau'], $Niveau) !== false) {
                    $SCORE = $SCORE * sqrt(1 / $R['niveau_nbr']);
                } else {
                    $SCORE = $SCORE * 0.2;
                }


                if ($TestSportNutri == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport = strtolower($sport);
                        if (strpos($R['sport_coaching_nutritionnel'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_nutritionnel_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
                if ($TestSportPerf == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport = strtolower($sport);
                        if (strpos($R['sport_coaching_sportif'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_sportif_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
            } elseif ($sport_souhaite_faire == '1') {
                if ($TestSportNutri == true) {
                    foreach ($sport_pratique as $sport) {
                        $sport = strtolower($sport);
                        if (strpos($R['sport_coaching_nutritionnel'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_nutritionnel_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
                if ($TestSportPerf == true) {
                    foreach ($sport_souhait as $sport) {
                        $sport = strtolower($sport);
                        if (strpos($R['sport_coaching_sportif'], $sport) !== false) {
                            $SCORE = $SCORE * sqrt(1 / $R['sport_coaching_sportif_nbr']);
                        } else {
                            $SCORE = $SCORE * 0.1;
                        }
                    }
                }
            }

            if ($PreferenceSexe !== 'les deux') {
                if (strtolower($PreferenceSexe) !== strtolower($R['sexe_coach'])) {
                    $SCORE = $SCORE * 0.5;
                }
            }


            //On recupere les Coachs qui correspondent aux crietes Primaire

            fin:
            $R['SCORE'] = $SCORE;
            if ($SCORE > 0) {
                $TabCoach[] = $R;
            }
            if ($debug) {
                var_dump($R);
            }
        }

        $sort = array_column($TabCoach, 'SCORE');
        array_multisort($sort, SORT_DESC, $TabCoach);
        if (sizeof($TabCoach) > 0) {
            while ($TabCoach[0]['SCORE'] < 90) {
                for ($i = 0; $i < sizeof($TabCoach); $i++) {
                    $TabCoach[$i]['SCORE'] = $TabCoach[$i]['SCORE'] * 1.1;
                }
            }
        }
        if ($debug) {
            var_dump($TabCoach);
        }
        $SelCoach = -1;
        $TabCoachFinal = array();
        $Nb = min(sizeof($TabCoach), 3);
        for ($i = 0; $i < $Nb; $i++) {
            $sql = "SELECT *,'" . $TabCoach[$i]['SCORE'] . "' AS SCORE FROM ccp.`admin` WHERE `idAdmin`=" . $TabCoach[$i]['idcoach'];
            $res2 = $em->getConnection()->FetchAll($sql);

            $TabCoachFinal[] = $res2[0];
            $TabCoachFinal[$i]['profil'] = $TabCoach[$i];

            if ($request->get('cidc') == $TabCoach[$i]['idcoach']) {
                $SelCoach = $i;
            }
        }

//        var_dump($TabCoachFinal);
        if ($SelCoach < 0 and $Nb > 0) {
            $SelCoach = 0;
        }

        //     var_dump($SelCoach);

        //   var_dump($this->FullUser['token_clt']);
        //exit;
        return $this->render('tout_est_possible/Coachs.twig', [
            'SelCoach' => $SelCoach,
            'Coachs' => $TabCoachFinal,
            'Token' => $this->FullUser['token_clt']
        ]);
    }
}