# Custom Coaching

Projet effectué dans le cadre de mon stage réalisé pendant ma formation de Développeur Web et Web Mobile. Sur le framework Symfony 4

Centre de formation : BeWeb Montpellier

Entreprise : Custom Coaching

Durée du stage : 2 mois

Objectifs :
-	Remplacer l’application mobile existante par une application web.
-	Mise en place d’une PWA (Progressive Web Application), afin que les clients puissent installer l’application sur leur téléphone mobile.

Contraintes :
-	Utilisation d’une base de données déjà existante avec des droit en lecture seulement
-	Utilisation d’une API réalisé par une société externe permettant l’écriture sur la base de données.

Branche du git comportant le travail réalisé :
-	Branche Dev
